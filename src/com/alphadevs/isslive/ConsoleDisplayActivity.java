package com.alphadevs.isslive;

import com.alphadevs.isslive.models.Console;
import com.alphadevs.isslive.models.ISSData;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class ConsoleDisplayActivity extends ListActivity{
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ISSData issData = ISSData.getInstance();
		Console console = issData.getConsoles().get(getIntent().getIntExtra("console_index", 0));
		this.setListAdapter(new SimpleAdapter(ConsoleDisplayActivity.this,console.getDisplaysMap(), R.layout.list_item, new String[]{"name"}, new int[]{R.id.name}));
	}
	
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(getApplicationContext(),SensorsDisplayActivity.class);
		intent.putExtra("console_index", getIntent().getIntExtra("console_index",0));
		intent.putExtra("console_display_index", position);
		startActivity(intent);
		super.onListItemClick(l, v, position, id);
	}

}
