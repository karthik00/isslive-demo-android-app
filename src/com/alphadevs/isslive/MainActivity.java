package com.alphadevs.isslive;



import org.json.JSONException;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.alphadevs.isslive.models.ISSData;

public class MainActivity extends ListActivity {
	
	// url to make request
    private static String url = "http://10.4.31.124:8000/api/metadata";
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		new AsyncTask<String, Void, JSONObject> (){
			@Override
			protected JSONObject doInBackground(String... urls) {
				JSONParser jsonParser = new JSONParser();
				JSONObject obj = jsonParser.getJSONFromUrl(urls[0]);
				return obj; 
			}

			@Override
			protected void onPostExecute(JSONObject result) {
				ISSData issData = ISSData.getInstance();
				try {
					issData.loadMetadata(result);
					MainActivity.this.setListAdapter(new SimpleAdapter(MainActivity.this, issData.getConsolesMap(), R.layout.list_item, new String[]{"name"}, new int[]{R.id.name}));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}}.execute(url);
 
		
    }
	
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(getApplicationContext(),ConsoleDisplayActivity.class);
		intent.putExtra("console_index", position);
		startActivity(intent);
		
		super.onListItemClick(l, v, position, id);
	}
	
}