package com.alphadevs.isslive.models;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

public class URLFactory {
	private static final String APP_ROOT = "appRoot";

	public static URL getURLForService(String service)
			throws FileNotFoundException, IOException {
		String serverRoot = getAppRoot();
		URL url = new URL(serverRoot);
		url = new URL(url, "api/" + service);
		return url;
	}

	public static String getAppRoot() throws FileNotFoundException, IOException {
		Properties props = new Properties();
		String file = ClassLoader.getSystemResource(
				"com/alphadevs/isslive/config.properties").getFile();
		props.load(new FileInputStream(file));
		return props.getProperty(APP_ROOT);
	}

}