package com.alphadevs.isslive.models;

import org.json.JSONException;
import org.json.JSONObject;


public class SensorData {
	String name;
	float value;
	float lastUpdated;
	String calibratedData;
	String alias;

	SensorData latestData;

	SensorData(JSONObject json) throws JSONException {
		this.name = (String) json.get("Name");
		this.value = (Float) json.get("Value");
		this.calibratedData = (String) json.get("CalibratedData");
		this.lastUpdated = (Float) json.get("LastUpdated");
		if (json.has("Alias"))
			this.alias = (String) json.get("Alias");
	}

	public String getName() {
		return name;
	}

	public float getValue() {
		return value;
	}

	public float getLastUpdated() {
		return lastUpdated;
	}

	public String getCalibratedData() {
		return calibratedData;
	}

	@Override
	public String toString() {
		return "name : " + name + " alias : " + alias + " value : " + value
				+ " calibratedData : " + calibratedData + " lastUpdated : "
				+ lastUpdated;
	}
}
