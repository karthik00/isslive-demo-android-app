package com.alphadevs.isslive;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.SimpleAdapter;

import com.alphadevs.isslive.models.ConsoleDisplay;
import com.alphadevs.isslive.models.ISSData;

public class SensorsDisplayActivity extends ListActivity{
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		ISSData issData = ISSData.getInstance();
		ConsoleDisplay consoleDisplay = issData.getConsoles().get(getIntent().getIntExtra("console_index", 0)).getDisplays().get(getIntent().getIntExtra("console_display_index", 0));
		this.setListAdapter(new SimpleAdapter(this,consoleDisplay.getSensorsMap(), R.layout.list_item, new String[]{"shortDesc","name","desc"}, new int[]{R.id.shortDesc,R.id.name,R.id.desc}));
	}
}
