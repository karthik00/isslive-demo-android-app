package com.alphadevs.isslive.models;

public class Sensor implements Searchable, ISSElement {
	String name;
	String alias;
	String shortDescription;
	String description;
	SensorData latestData;

	Sensor(String name, String alias, String shortDesc, String desc) {
		this.name = name;
		this.shortDescription = shortDesc;
		this.description = desc;
	}

	public void setLatestData(SensorData data) {
		this.latestData = data;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public String getDescription() {
		return description;
	}

	public SensorData getLatestData() {
		return latestData;
	}

	@Override
	public boolean contains(String query) {
		return name.toLowerCase().contains(query)
				|| shortDescription.toLowerCase().contains(query)
				|| description.toLowerCase().contains(query)
				|| (alias != null && alias.toLowerCase().contains(query));
	}

	@Override
	public String getName() {
		return name;
	}
}
