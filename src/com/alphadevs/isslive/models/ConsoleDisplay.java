package com.alphadevs.isslive.models;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class ConsoleDisplay implements Searchable, ISSElement {
	String name;
	ArrayList<Sensor> sensors = new ArrayList<Sensor>();
	String serverURL;
	Console parentConsole;

	public ArrayList<Sensor> getSensors() {
		return sensors;
	}

	public void addSensors(ArrayList<Sensor> sensors) {
		this.sensors.addAll(sensors);
	}

	public ConsoleDisplay(String name, Console parent) {
		this.name = name;
		this.parentConsole = parent;
	}

	public ArrayList<SensorData> updateAndGetLatest()
			throws FileNotFoundException, IOException, JSONException {
		updateSensors();
		return getAllSensorData();
	}

	public void updateSensors() throws FileNotFoundException, IOException, JSONException {
		String serviceName = parentConsole.getName() + "/" + name;
		// TODO remove if check once
		// http://localhost:8000/api/cronus/video_communication problem resolved

		if (name.equals("video_communication"))
			return;
		URL url = URLFactory.getURLForService(serviceName);
		URLConnection connection = url.openConnection();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		String sensorDataString = in.readLine();
		JSONArray jsonArray = new JSONArray(sensorDataString); 
		for (int i=0; i<jsonArray.length();i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			SensorData sensorData = new SensorData(jsonObject);
			Sensor sensor = getSensorByname(sensorData.getName());
			sensor.setLatestData(sensorData);
		}
	}

	private Sensor getSensorByname(String sensorName) {
		for (Sensor sensor : sensors) {
			if (sensor.getName().equals(sensorName))
				return sensor;
		}
		throw new RuntimeException("No sensor named " + sensorName
				+ " is part of Console Display " + name);
	}

	@Override
	public boolean contains(String query) {
		return name.toLowerCase().contains(query);
	}

	/*
	 * returns the sensordata for all the sensors within the display
	 */
	public ArrayList<SensorData> getAllSensorData() {
		ArrayList<SensorData> result = new ArrayList<SensorData>();
		for (Sensor sensor : sensors) {
			result.add(sensor.getLatestData());
		}
		return result;
	}

	/*
	 * returns sensordata for a sensor
	 */
	public SensorData getSensorData(String sensorName) {
		Sensor sensor = getSensorByName(sensorName);
		return sensor.getLatestData();
	}

	public Sensor getSensorByName(String sensorName) {
		for (Sensor sensor : sensors) {
			if (sensor.getName().equals(sensorName))
				return sensor;
		}
		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	public List<Map<String, String>> getSensorsMap() {
		ArrayList<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for(final Sensor sensor : sensors){
			result.add(new HashMap<String, String>(){
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				{
					this.put("shortDesc",sensor.getShortDescription());
					this.put("name", sensor.getName());
					this.put("desc", sensor.getDescription());					
				}
			});
		}
		return result;
	}
}
