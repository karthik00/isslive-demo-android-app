package com.alphadevs.isslive.models;

public interface Searchable {
	public boolean contains(String query);
}
