package com.alphadevs.isslive.models;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ISSData {
	
	private static ISSData instance;
	
	public static ISSData getInstance(){
		if(instance==null){
			instance=new ISSData();
		}
		return instance;
	}
	
	ArrayList<Console> consoles = new ArrayList<Console>();

	/*
	 * loads all the metadata info from server . does not update the sensors
	 */
	
	
	public void loadMetadata(JSONObject o) throws JSONException {
		Iterator<String> iterator = o.keys();
		while (iterator.hasNext()) {
			String consoleName = iterator.next();
			Console console = new Console(consoleName);
			consoles.add(console);
			JSONObject screens = (JSONObject) o.get(consoleName);
			Iterator<String> itt = screens.keys();
			ArrayList<ConsoleDisplay> displays=new ArrayList<ConsoleDisplay>();
			while (itt.hasNext()) {
				String consoleDisplayName = itt.next();
				ConsoleDisplay display = new ConsoleDisplay(consoleDisplayName,console);
				displays.add(display);
				JSONArray sensorArray = screens.getJSONArray(consoleDisplayName);
				ArrayList<Sensor> sensors = new ArrayList<Sensor>();
				for(int i=0;i<sensorArray.length();i++){
					JSONObject sensorObj = sensorArray.getJSONObject(i);
					String sensorName = (String) sensorObj.get("name");
					// shortDesc and short_desc for some sensors . replaced
					// short_desc with shortDesc in sample file
					String shortDesc = (String) sensorObj.get("shortDesc");
					String description = (String) sensorObj.get("desc");
					String alias = null;
					if (sensorObj.has("alias"))
						alias = (String) sensorObj.get("alias");
					Sensor sensor = new Sensor(sensorName, alias, shortDesc,description);
					sensors.add(sensor);
				}
				display.addSensors(sensors);
			}
			console.setDisplays(displays);
		}
	}

	/*
	 * Search for an ISSElement in the system
	 */
	public ArrayList<ISSElement> search(String query) {
		query = query.toLowerCase();
		ArrayList<ISSElement> result = new ArrayList<ISSElement>();
		for (Console console : consoles) {
			if (console.contains(query))
				result.add(console);
			for (ConsoleDisplay display : console.getDisplays()) {
				if (display.contains(query))
					result.add(display);
				for (Sensor sensor : display.getSensors()) {
					if (sensor.contains(query))
						result.add(sensor);
				}
			}
		}
		return result;
	}

	public SensorData getSensorData(String consoleName, String displayName,
			String sensorName) {
		Console console = getConsoleByName(consoleName);
		return console.getSensorData(displayName, sensorName);
	}

	/*
	 * Updates all the sensors
	 */
	public void updateAll() throws FileNotFoundException, IOException, JSONException {
		for (Console console : consoles) {
			console.updateAll();
		}
	}

	public Console getConsoleByName(String consoleName) {
		for (Console console : consoles) {
			if (console.getName().equals(consoleName))
				return console;
		}
		return null;
	}

	/*
	 * Returns all sensor data for the specified display
	 */
	public ArrayList<SensorData> getAllSensorDataofDisplay(String displayName)
			throws FileNotFoundException, IOException, JSONException {
		for (Console console : consoles) {
			for (ConsoleDisplay display : console.getDisplays()) {
				if (display.getName().equals(displayName))
					return display.updateAndGetLatest();
			}
		}
		return null;
	}

	public ArrayList<Console> getConsoles() {
		return consoles;
	}
	
	public List<Map<String, String>> getConsolesMap() {
		ArrayList<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for(final Console console : consoles){
			result.add(new HashMap<String, String>(){
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				{
					this.put("name", console.getName());
					
				}
			});
		}
		return result;
	}
}