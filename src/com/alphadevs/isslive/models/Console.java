package com.alphadevs.isslive.models;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

public class Console implements Searchable, ISSElement {
	String name;
	ArrayList<ConsoleDisplay> consoleDisplays = new ArrayList<ConsoleDisplay>();

	public ArrayList<ConsoleDisplay> getDisplays() {
		return consoleDisplays;
	}

	Console(String name) {
		this.name = name;
	}

	public void setDisplays(ArrayList<ConsoleDisplay> consoleDisplays) {
		this.consoleDisplays = consoleDisplays;
	}

	@Override
	public boolean contains(String query) {
		return name.toLowerCase().contains(query);
	}

	public ArrayList<SensorData> getAllSensorData() {

		ArrayList<SensorData> result = new ArrayList<SensorData>();
		for (ConsoleDisplay display : consoleDisplays) {
			result.addAll(display.getAllSensorData());
		}
		return result;
	}

	@Override
	public String getName() {
		return name;
	}

	public void updateAll() throws FileNotFoundException, IOException, JSONException {
		for (ConsoleDisplay display : consoleDisplays) {
			display.updateSensors();
		}
	}

	public ConsoleDisplay getDisplayByName(String displayName) {
		for (ConsoleDisplay display : consoleDisplays) {
			if (display.getName().equals(displayName))
				return display;
		}
		return null;
	}

	public SensorData getSensorData(String displayName, String sensorName) {
		ConsoleDisplay display = getDisplayByName(displayName);
		return display.getSensorData(sensorName);
	}

	public List<Map<String, String>> getDisplaysMap() {
		ArrayList<Map<String, String>> result = new ArrayList<Map<String, String>>();
		for(final ConsoleDisplay consoleDisplay : consoleDisplays){
			result.add(new HashMap<String, String>(){
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;
				{
					this.put("name", consoleDisplay.getName());
				}
			});
		}
		return result;
	}
}
